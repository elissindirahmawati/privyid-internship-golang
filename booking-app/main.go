package main

import (
	"fmt"
	"time" 
	"sync"
	// "strings" ->package ini udh gadigunain pas udh diubah jadi bentuk maps
	"booking-app/helper" //perlu import supaya ke detect
	//"strconv" ini dibutuhin nya kalo pake maps. karena data dikonversi ke bentuk lain atau bentuk string
)

	// before. bawahnya after. conferenceName :=  "Go Conference" // cara lain deklarasi variabel. tpi yg ini gabisa atur const let atau var nya 
	var conferenceName = "Go Conference"
	const conferenceTickets int = 50 //constant nilainya ga akan berubah
	var remainingTickets uint = 50 //tipe data integer yang positive
	
	//var bookings [50]string // ini array karena diatur panjang arraynya
	//var bookings []string diubah jadi bentuk maps// ini slices
	// bookings:= []string{} // cara lain penulisannya
	// var bookings = make([]map[string]string,0) ini bentuk map. diubah jadi bentuk struct kyk dibawah ini
	var bookings = make([]UserData, 0)

	type UserData struct {
		firstName string
		lastName string
		email string
		numberOfTickets uint
		
	}
var wg =  sync.WaitGroup{}
	func main() {
	//var conferenceName =  "Go Conference" //deklarasi variabel. variabelnya harus idgunain kalo engga bakal error
	
	greetUsers()

	//fmt.Printf("conferenceTickets is %T, remainingTicket is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName) // ngecek tipe data saat print pakai %T

	
	//for {  //dihapussoalnya 1 user 1xpesen abis itu exit
		// manggil variabel dari fungsi untuk user input
		firstName, lastName, email, userTickets := getUserInput()
		// manggil fungsi untuk validasi
		isValidName, isValidEmail, isValidTicketNumber := helper.ValidateUserInput(firstName, lastName, email, userTickets, remainingTickets)	

		if isValidName && isValidEmail && isValidTicketNumber { //if else buat nentuin apa bisa booking sejumlah tiket
		bookTicket(userTickets, firstName, lastName, email)
		
		wg.Add(1) //karena go nya cuma 1 maka isi 1
		go sendTicket(userTickets, firstName, lastName, email)

		//call function	
		firstNames := getFirstNames() // diambil dari function getFirstNames
		fmt.Printf("The first namesof bookings are: %v\n", firstNames)
		
				if remainingTickets == 0  {
					//end program
					fmt.Println("Our conference is booked out. Come back next year.")
					//break //untuk memutus ifnya
				}	
		} else {
			if !isValidName {
				fmt.Println("First name or last name you entered is too short")
			}
			if !isValidEmail {
				fmt.Println("email address you entered doesn't contain @ sign")
			}
			if !isValidTicketNumber {
				fmt.Println("Number of tickets you entered is invalid")
			} // ga pake  else soalnya mau semua ditampilin. kalo else if kan harus terpenuhi salah satunya
			//fmt.Printf("Your input data is invalid, try again")
			//continue //supaya bisa lanjut ga lngsg keptus
		}
		wg.Wait()
		}
		/* contoh switch case
		city := "London"

		switch  city {
		case "New York":
			//execute code for booking new york conference
		case "Singapore", "Hong Kong":
		case "London", "Berlin":
		case "Mexico City" :
		default:
			fmt.Println("No Valid city selected")
		}*/
//}
//function 
func greetUsers() { 
	fmt.Printf("Welcome to %v, boking application\n", conferenceName)
	fmt.Printf("We have total of  %v tickets and %v are still available\n", conferenceTickets, remainingTickets) // ini pake format output. pake printf
	fmt.Println("Get your tickets here to attend")

} 
func getFirstNames() []string { // yg awal itu input parameter. yg kedua output parameter buat return
	firstNames := []string{} //slices buat firstName doang
			for _, booking := range bookings {//logic ini disimpan di dalam variabel baru yaitu booking. ambil data dari bookings
				//var names = strings.Fields(booking) //split. dengan syntax ini akan membagi array dengan space. dan disimpan dalam variable names
				// ini ambil data dari maps booking
				//firstNames = append(firstNames, booking["firstName"] )// menyimpan hasil split firstname di variable firstName
				//ini ambil data dari struct booking
				firstNames = append(firstNames, booking.firstName)
			}
				return firstNames 
}



func getUserInput()(string, string, string, uint) {
	var firstName string
		var lastName string
		var email string
		var userTickets uint
	//ask user for their name 
		fmt.Println("Enter your first name: ")
		fmt.Scan(&firstName)

		fmt.Println("Enter your last name: ")
		fmt.Scan(&lastName)

		fmt.Println("Enter your email address: ")
		fmt.Scan(&email)

		fmt.Println("Enter number of tickets: ")
		fmt.Scan(&userTickets)

		return firstName, lastName, email, userTickets
}

func bookTicket (userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets
	// create a map for a user
	// var userData = make(map[string]string) // map[the key]the value ini bentuk map
	//bawah ini bentuk struct
	 var userData = UserData {
		firstName: firstName,
		lastName: lastName,
		email: email,
		numberOfTickets: userTickets,
	 }
	/* ini buat bentuk yg map
	userData["firstName"] = firstName // key bebas dinamain apa aka
	userData["lastName"] = lastName
	userData["email"] = email
	userData["numberOfTickets"] = strconv.FormatUint(uint64(userTickets), 10) //pake package strconvert. mengubah int ke string. karena map hanya bisa punya 1 tipe data */
			//bookings[0] = firstName + " " +  lastName // kalo pakai array
			bookings = append(bookings, userData)
		fmt.Printf("List of bookings is %v\n", bookings)
			fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
			fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)	
}
func sendTicket(userTickets uint, firstName string, lastName string, email string) {
	time.Sleep (20 * time.Second) // buat atur waktu jeda
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName) //Sprint itu print dan menyimpannya ke dalam string variabel
	fmt.Println("###################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("###################")
	wg.Done() // untuk kasih tau kalo udh selesai jadi nunggunya udahan
	} 