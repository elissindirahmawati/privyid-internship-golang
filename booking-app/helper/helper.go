//pertamakali ygang harusidlakukan adalah pilih package apa
package helper

import "strings"

func ValidateUserInput(firstName string, lastName string, email string, userTickets uint, remainingTickets uint) (bool, bool, bool) { //exporting package. pastiin huruf awalnya kapital 
	// ngecek validasi data yg diinputkan user
	//contoh logical 
		//isValidCity := city == "Singapore" || city == "London"
		// isValidCity := city != "Singapore" || city != "London" //gaboleh sama dengan. sama aja kayak 
		//!isValidCity
	isValidName := len(firstName) >= 2 &&  len(lastName) >= 2
	isValidEmail :=  strings.Contains(email, "@")// harus contain @
	isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets 
	return isValidName, isValidEmail, isValidTicketNumber	
}